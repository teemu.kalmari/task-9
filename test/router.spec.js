const expect = require("chai").expect;
const router = require("../src/router");
const PORT = 3000;
const HOST = "127.0.0.1";
// SCHEME or PROTOCOL
const baseUrl = `http://${HOST}:${PORT}`;

describe("Test routes", () => {
    let server;
    before("Start server", (done) => {
        server = router.listen(
            PORT,
            HOST,
            () => done()
        );
    });

    describe("Routes", () => {
        it("Can GET welcome message", async () => {
            const response = await fetch(baseUrl + "/");
            const response_msg = await response.text();
            expect(response_msg).to.equal("welcome!");
        });

        it("Can GET sum of two numbers", async () => {
            const endpoint = baseUrl + "/add";
            const getResponse = (a, b) =>
                new Promise(async (resolve, reject) => {
                    const query = `${endpoint}?a=${a}&b=${b}`; // http://example.com/endpoint?a=1&b=2
                    const response = await (await fetch(query)).text();
                    resolve(response);
                });

            expect(await getResponse(1, 2)).to.equal("3");
            expect(await getResponse(0, 0)).to.equal("0");
            expect(await getResponse(-1, 2)).to.equal("1");
            expect(await getResponse(0.1, 0.2)).to.equal("0.3");
        });

        it("Can GET product of two numbers", async () => {
            const endpoint = baseUrl + "/mul";
            const getResponse = (a, b) =>
                new Promise(async (resolve, reject) => {
                    const query = `${endpoint}?a=${a}&b=${b}`; // http://example.com/endpoint?a=1&b=2
                    const response = await (await fetch(query)).text();
                    resolve(response);
                });

            expect(await getResponse(2, 3)).to.equal("6");
            expect(await getResponse(-2, 3)).to.equal("-6");
            expect(await getResponse(0.1, 0.2)).to.equal("0.02");
        });

        it("Changed behaviour of REST API", async () => {
            const endpoint = baseUrl + "/add";
            const getResponse = (a, b) =>
                new Promise(async (resolve, reject) => {
                    const query = `${endpoint}?a=${a}&b=${b}`;
                    const response = await (await fetch(query)).text();
                    resolve(response);
                });
        
            // Break the API by changing it
            expect(await getResponse("1", 2)).to.equal("3");
        
          
        });
        
       
    });

    after("Close server", () => {
        server.close();
    });
});
