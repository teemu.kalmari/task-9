const express = require('express');
const router = express();

// router endpoint
router.get("/", (req, res) => res.send("welcome!"));

// router endpoint to add two numbers together. Query parameters are "a" and "b".
router.get("/add", (req, res) => {
    try {
        const sum = req.query.a + req.query.b;
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});
router.get('/mul', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    if (isNaN(a) || isNaN(b)) {
      res.status(400).send('Invalid input');
    } else {
      res.send((a * b).toString());
    }
  });
  

// Endpoint 3 POST http://localhost:3000/webhook-update
/** Proper JSON REST API Middlewares */
router.use(express.urlencoded({ extended: true }));
router.use(express.json());
// post here

module.exports = router;
