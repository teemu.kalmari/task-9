const express = require("express");
const port = 3000;
const app = express();

const util = require('util');

app.get("/", (req, res) => res.send("welcome"));

/** Add two numbers */
app.get("/add", (req, res) => {
    try {
        const sum = parseInt(req.query.a) + parseInt(req.query.b);
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500); // Server internal error
    }
});
/** Multiply two numbers */
app.get("/mul", (req, res) => {
    try {
        const product = parseInt(req.query.a) * parseInt(req.query.b);
        res.send(product.toString());
    } catch (e) {
        res.sendStatus(500); // Server internal error
    }
});


// localhost:3000/update
app.get("/update", async (req, res) => {
   const exec = util.promisify(require('child_process').exec);
    const {stdout, stderr} = await exec("./update.sh");
    console.log('stdout:', stdout);
    console.log('stderr:' , stderr);
    process.exit(1);
});

if (process.env.NODE_ENV === "test") {
    module.exports = app;
}

if (!module.parent) {
    app.listen(port, () => console.log(`Server running at localhost:${port}`));
}